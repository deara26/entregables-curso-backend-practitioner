package com.techuniversity.emp.model;

public class Formacion {
    private String fecha;
    private String titulo;
    public Formacion(String fecha, String titulo) {
        this.fecha = fecha;
        this.titulo = titulo;
    }

    public Formacion() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Formacion{");
        sb.append("fecha='").append(fecha).append('\'');
        sb.append(", titulo='").append(titulo).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
