package com.techuniversity.prod.productos;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    ProductoRepositorio productoRepositorio;

    public List<ProductoModel> findAll(){

        return productoRepositorio.findAll();
    }

    public Optional<ProductoModel> findById(String id){

        return  productoRepositorio.findById(id);
    }

    public ProductoModel save(ProductoModel producto) {

        return productoRepositorio.save(producto);
    }

    public boolean deleteProducto(ProductoModel producto){
        try{
            productoRepositorio.delete(producto);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
    public List<ProductoModel> findPaginado(int page){
        Pageable pageable = PageRequest.of(page,3);
        Page<ProductoModel> pages = productoRepositorio.findAll(pageable);
        List<ProductoModel> productos = pages.getContent();
        return productos;
    }
}
