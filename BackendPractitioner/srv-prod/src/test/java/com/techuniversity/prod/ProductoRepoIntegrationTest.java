package com.techuniversity.prod;

import com.techuniversity.prod.controllers.ProductoController;
import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoRepositorio;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@SpringBootTest
public class ProductoRepoIntegrationTest {
    @Autowired
    ProductoRepositorio productoRepositorio;

    @Test
    public void testFindAll () {
        List<ProductoModel> productos = productoRepositorio.findAll();
        assertTrue(productos.size() > 0);
    }
    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws  Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null,headers);
        ResponseEntity<String> response = testRestTemplate.exchange(
        crearUrlConPuerto("/productos/productos/"),
                HttpMethod.GET,entity,String.class
        );
        //String expected = "<JSON DEL POSTMAN>"
       // JSONAssert.assertEquals(expected,response.getBody(),false);
    }
    private String crearUrlConPuerto(String url) {
        return "http://localhost" + port + url;
    }
}
