package com.techuniversity.emp.model;

import java.util.ArrayList;

public class Empleado {

    public Empleado() {
    }

    public Empleado(Integer id, String nombre, String apellidos,String email,ArrayList<Formacion> formaciones) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.apellidos = apellidos;
        this.formaciones = formaciones;
    }

    private  Integer id;
    private String nombre;
    private String apellidos;
    private String email;
    private ArrayList<Formacion> formaciones;

    public ArrayList<Formacion> getFormaciones() {
        return formaciones;
    }

    public void setFormaciones(ArrayList<Formacion> formaciones) {
        this.formaciones = formaciones;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Empleado{");
        sb.append("id=").append(id);
        sb.append(", nombre='").append(nombre).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", apellidos='").append(apellidos).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
